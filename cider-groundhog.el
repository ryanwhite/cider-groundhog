;;; cider-groundhog --- Stop jumping to another buffer or navigating to that s-exp you want to run repeatedly.

;; Copyright (C) 2019 Ryan White <ryan@cognician.com>

;; Authors: Ryan White <ryan@cognician.com>
;; Keywords: cider, convenience
;; Package-Version: 20191012.1611
;; Version: 0.1.0
;; URL: https://gitlab.com/ryanwhite/cider-groundhog
;; License: GNU GPL v3: https://www.gnu.org/licenses/gpl-3.0.en.html
;; 
;;; Commentary:

;; Do you frequently:
;; -> do cider-pprint-eval-last-sexp (C-c C-p)
;; -> tweak some code, do cider-load-buffer (C-c C-k)
;; -> go back to the test function
;; -> do cider-pprint-eval-last-sexp (C-c C-p)

;; It's like Groundhog day - and you're trapped, doing the same things
;; until you understand what you're doing and get it right.

;; this normally means you've got to tweak code and go back to a form
;; OR
;; keep a separate buffer open for just this purpose.

;; Now, you can keep the shenanigans to a minimum:

;; When you start up a cider-connection - the key combo
;; C-c C-g is bound to 'cider-groundhog.
;; activate the groundhog like this:
;; (defn foo []
;;   "hello there")
;;
;; (foo) ;;<<groundhog
;;
;; ^^ see there - put ";;<<groundhog after your target s-exp.
;;
;; First run: put your cursor on the "<<groundhog" bit
;; and hit C-c C-g
;; it'll run like you expect.
;; from there, you can be anywhere else in the file, and
;; when you hit C-c C-g, Emacs will find its way to
;; the right point and run (foo) for you.
;;
;; It'll even work when you're in another file, tweaking code THERE
;; and hitting the combo will go to the last <<groundhog that was
;; marked as the current one.  This is done transparently, so there's
;; no disturbing "jumping around".
;;
;; If you've got <<groundhog s in several files, whichever one
;; you put your cursor on and run C-c C-g on is the "current" one.
;;
;; hopefully shaving seconds off your REPL cycle ;)

;;; Code:

;; Global variable...
(defvar cider-groundhog-buffer)

(defgroup cider-groundhog nil
	"Stop jumping to another buffer or navigating to that s-exp you want to run repeatedly."
	:group 'cider)

(defcustom cider-groundhog-marker "<<groundhog"
	"How cider-groundhog knows which s-exp to look for."
	:type 'string
	:group 'cider-groundhog)

(defcustom cider-groundhog-key-sequence (kbd "C-c C-g")
	"How cider-groundhog knows which s-exp to look for."
	:type 'key-sequence
	:group 'cider-groundhog)

;; the meat 'n potatoes
(defun cider-groundhog ()
	"Helps you run things over and over and over.."
	(interactive)
	(if (fboundp 'cider-pprint-eval-last-sexp)
			(progn
				(if (or (string= cider-groundhog-marker (current-word))
								(not (boundp 'cider-groundhog-buffer)))
						(setq cider-groundhog-buffer (current-buffer)))
				(save-excursion
					(let ((return-buffer (current-buffer)))
						(switch-to-buffer (or cider-groundhog-buffer (current-buffer)))
						(goto-char (point-min))
						(if (search-forward cider-groundhog-marker nil t)
								(progn
									(search-backward cider-groundhog-marker nil t)
									(cider-pprint-eval-last-sexp))
							(message (concat "No " cider-groundhog-marker " found.")))
						(switch-to-buffer return-buffer))))
		(message "cider-pprint-eval-last-sexp does not look to be defined. Are you using Cider, and connected?")))

;; adding shortcut when cider is active.
(add-hook 'cider-mode-hook (lambda () (local-set-key cider-groundhog-key-sequence 'cider-groundhog)))

(provide 'cider-groundhog)
;;; cider-groundhog.el ends here
